# Clone this repo
    git clone git@gitlab.com:sazo/my-awesome-git.git

# Make a Commit and push
    # Make a new file
    touch test.php
    
    # See unversioned files and unstaged changes
    git status
    
    # Add the file to revision
    git add test.php
    
    # Staged and commit changes
    git commit -am "Added test.php"

    # Pushed the change to remote
    git push

# Make a branch named iss53 from master and merge it back again
    
    # Make a branch
    git checkout master
    git checkout -b iss53
    
    # Make a change to test.php
    
    # Commit changes
    git commit -am "I am awesome"
    
    # commit and push the change
    git push
    
    # merge iss53 into master
    git checkout master
    git merge iss53
    
    # push master to remote
    git push
    
# Make a merge conflict and resolve it
    # Stephan : Change test.php to bee 'Stephan is the best'
    git commit -am "Stephan is the best"

    # Morten : Change test.php to bee 'Morten is the best'
    git commit -am "Morten is the best"
    
    # Stephan : push to remote
    git push
    
    # Morten : push to remote and gets rejected because of missing commit
    git push
    
    # Morten : pulls and gets a conflict
    git pull
    
    # Morten : resolves conflict with favorite editor and commits it
    git commit -am "Resolve conflict"
    git push
    
    
# Contribute

 - Daniel Fly
